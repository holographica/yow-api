﻿using System.Linq;
using LiteDB;

public static class RandomChars
{
    public static string RandomString(int count)
    {
        var random = new System.Random();
        const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        var newString = new string(Enumerable.Repeat(chars, count).Select(s => s[random.Next(s.Length)]).ToArray());
        
        using var db = new LiteDatabase("./planets.db");
        // Get a collection (or create, if doesn't exist)
        if (db.GetCollection<Game>("Games").Query().Where(g => g.ID == newString).Count() > 0) RandomString(count);
        return newString;
    }    
    
    public static int RandomInt(int count)
    {
        var random = new System.Random();
        const string chars = "0123456789";
        var newString = new string(Enumerable.Repeat(chars, count).Select(s => s[random.Next(s.Length)]).ToArray());

        return int.Parse(newString);
    }
}
