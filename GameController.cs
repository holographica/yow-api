using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LiteDB;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

// ReSharper disable NotAccessedField.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace HologramxApi.Controllers
{
    [ApiController]
    public class GameController : ControllerBase
    {
        // this gives the list of all games
        [HttpGet("api")]
        public ActionResult<string> GetGames()
        {
            using var db = new LiteDatabase("./planets.db");
            // Get a collection (or create, if doesn't exist)
            var col = db.GetCollection<Game>("Games");
            var list = col.FindAll().ToList();
            return JsonConvert.SerializeObject(list); //list of games
        }

        // moved new game creation here to avoid creating multiple empty games and the need to also provide the game ID 
        [HttpGet("api/games/{username}")]
        public ActionResult<string> Play(string username)
        {
            using var db = new LiteDatabase("./planets.db");
            // Get a collection (or create, if doesn't exist)
            var games = db.GetCollection<Game>("Games");
            var game = new Game
            {
                ID = RandomChars.RandomString(10), Description = "Questions about planets", Username = username,
                ImageUrl = "https://cdn3.iconfinder.com/data/icons/venus-basic-1/48/001_005_earth_globe_planet_language-512.png",
                UserImageUrl = "https://www.pavilionweb.com/wp-content/uploads/2017/03/man-300x300.png", NumOfQuestions = 4, AverageRating = 3
            };

            // Insert new game (Id will be auto-incremented)
            games.Insert(game);

            var extendedGames = db.GetCollection<ExtendedGame>("ExtendedGames");
            var extendedGame = new ExtendedGame(db) {ID = game.ID};
            extendedGames.Insert(extendedGame);
            var json = JsonConvert.SerializeObject(extendedGame);
            return json;
        }

        [HttpPost("api/answers")]
        public ActionResult<string> Answer([FromForm] Dictionary<string, string> answerString)
        {
            using var db = new LiteDatabase("./planets.db");
            var col = db.GetCollection<GameReport>("Answers");
            col.Insert(JsonConvert.DeserializeObject<GameReport>(answerString.First().Value));
            return "Done";
        }

        [HttpGet("api/upload")]
        public ActionResult<string> UploadQuestions()
        {
            using var db = new LiteDatabase("./planets.db");
            var col = db.GetCollection<Question>("Questions");
            var question1 = new Question
            {
                ID = RandomChars.RandomString(8), Answer = 0, AnswerVariants = new[] {"Galileo Galilei", "Nicolaus Copernicus", "Johannes Kepler", "Isaac Newton"},
                Title = "Who discovered the four main moons of Jupiter in 1610?", Type = (int) QuestionTypes.OnePlanet, Planets = new[] {"Jupiter"}, Explanation = ""
            };
            var question2 = new Question
            {
                ID = RandomChars.RandomString(8), Answer = 3, AnswerVariants = new[] {"1", "2", "3", "4"},
                Title = "What is Mars from the Sun?", Type = (int) QuestionTypes.OnePlanet, Planets = new[] {"Mars"}, Explanation = ""
            };
            var question3 = new Question
            {
                ID = RandomChars.RandomString(8), Answer = 2, AnswerVariants = new[] {"A", "B", "C", "D"}, Explanation = "",
                Title = "Which planet is the biggest in the Solar System?", Type = (int) QuestionTypes.FourPlanets, Planets = new[] {"Mars", "Saturn", "Jupiter", "Neptune"}
            };
            var question4 = new Question
            {
                ID = RandomChars.RandomString(8), Answer = 1, AnswerVariants = new[] {"A", "B", "C", "D"}, Explanation = "",
                Title = "Which planet has rings?", Type = (int) QuestionTypes.FourPlanets, Planets = new[] {"Mars", "Saturn", "Jupiter", "Neptune"}
            };

            col.Insert(question1);
            col.Insert(question2);
            col.Insert(question3);
            col.Insert(question4);
            return "Done";
        }
    }
}

public class Game
{
    public string ID { get; set; }
    public string Description { get; set; }
    public string ImageUrl { get; set; }
    public string Username { get; set; }
    public string UserImageUrl { get; set; }
    public int NumOfQuestions { get; set; }
    public int AverageRating { get; set; }

    public DateTime CreatedAt;

    public Game()
    {
        CreatedAt = DateTime.Now;
    }
}

public class ExtendedGame
{
    public string ID { get; set; }
    public string GameQuestions { get; }

    public ExtendedGame(ILiteDatabase db)
    {
        GameQuestions = GetQuestions(db);
    }

    private static string GetQuestions(ILiteDatabase db)
    {
        var questionsCol = db.GetCollection<Question>("Questions");
        var questions = questionsCol.Query().Where(q => q.Type == (int) QuestionTypes.FourPlanets).ToList();
        questions.AddRange(questionsCol.Query().Where(q => q.Type == (int) QuestionTypes.OnePlanet).ToList());
        var json = JsonConvert.SerializeObject(questions);
        return json;
    }
}

public enum QuestionTypes
{
    OnePlanet,
    FourPlanets
}

public class Question
{
    public string ID { get; set; }
    public int Type { get; set; }
    public string Title { get; set; }
    public string Explanation { get; set; }
    public int Answer { get; set; }
    public string[] AnswerVariants { get; set; }
    public string[] Planets { get; set; } // this simulates addressables keys, for now it's regular prefabs

    // public QuestionOptions QuestionOptions { get; set; }
}

// public class QuestionOptions
// {
//     public string Seconds { get; set; }
//     public bool AnsweredCorrectly { get; set; }
//     public int Order { get; set; }
// }

public class GameReport
{
    public string GameID { get; set; }
    public List<Answer> Answers;

    public GameReport(string report)
    {
        Answers = JsonConvert.DeserializeObject<List<Answer>>(report);
    }
}

public class Answer
{
    public string QuestionID { get; set; }
    public int Seconds { get; set; }
    public bool IsCorrect { get; set; }
    public int AnswerIndex { get; set; }
}